import os
from abc import ABC, abstractmethod

from FileDetails import FileDetails


class FilenameGenerator(ABC):
    @abstractmethod
    def next(self, file_detail: FileDetails) -> str:
        pass


class CounterPrefixPathGenerator(FilenameGenerator):
    def __init__(self, target_dir_path: str, keep_name: bool, start_number: int = 0):
        self.target_dir_path = target_dir_path
        self.keep_name = keep_name
        self.counter = start_number

    def next(self, file_detail: FileDetails) -> str:
        new_filename = self.__get_next_name(file_detail)
        new_file_path = os.path.join(self.target_dir_path, new_filename)

        # if filename still taken add suffix
        while os.path.exists(new_file_path):
            new_filename = self.__get_next_name(file_detail)
            new_file_path = os.path.join(self.target_dir_path, new_filename)

        return new_file_path

    def __get_next_name(self, file_detail: FileDetails) -> str:
        filename, extension = os.path.splitext(file_detail.filename)
        if self.keep_name:
            new_filename = str(self.counter) + "_" + filename + extension
            self.counter += 1
            return new_filename
        else:
            new_filename = str(self.counter) + extension
            self.counter += 1
            return new_filename

