import os


class FileDetails:
    def __init__(self, filename, directory_path):
        self.filename = filename
        self.directory_path = directory_path
        self.full_path = os.path.join(directory_path, filename)

    def __str__(self):
        return f"{self.filename}, in_directory={self.directory_path})"

    def get_modification_time(self) -> float:
        return os.path.getmtime(os.path.join(self.directory_path, self.filename))