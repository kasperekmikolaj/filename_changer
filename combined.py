import argparse
import os
import re
import shutil
import sys
from abc import ABC, abstractmethod


class FileDetails:
    def __init__(self, filename, directory_path):
        self.filename = filename
        self.directory_path = directory_path
        self.full_path = os.path.join(directory_path, filename)

    def __str__(self):
        return f"{self.filename}, in_directory={self.directory_path})"

    def get_modification_time(self) -> float:
        return os.path.getmtime(os.path.join(self.directory_path, self.filename))


class FilenameGenerator(ABC):
    @abstractmethod
    def next(self, file_detail: FileDetails) -> str:
        pass


class CounterPrefixPathGenerator(FilenameGenerator):
    def __init__(self, target_dir_path: str, keep_name: bool, start_number: int = 0):
        self.target_dir_path = target_dir_path
        self.keep_name = keep_name
        self.counter = start_number

    def next(self, file_detail: FileDetails) -> str:
        new_filename = self.__get_next_name(file_detail)
        new_file_path = os.path.join(self.target_dir_path, new_filename)

        # if filename still taken add suffix
        while os.path.exists(new_file_path):
            new_filename = self.__get_next_name(file_detail)
            new_file_path = os.path.join(self.target_dir_path, new_filename)

        return new_file_path

    def __get_next_name(self, file_detail: FileDetails) -> str:
        filename, extension = os.path.splitext(file_detail.filename)
        if self.keep_name:
            new_filename = str(self.counter) + "_" + filename + extension
            self.counter += 1
            return new_filename
        else:
            new_filename = str(self.counter) + extension
            self.counter += 1
            return new_filename


#todo
# if prefixed with number_ do not add another prefix
# when date sorting add option to add date as prefix
def query_yes_no(question, default="no"):
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")


def change_filenames(flat_dir: bool, sort_by_date: bool, start_number: int, keep_filenames: bool):
    directory_path = get_path_of_current_dir()

    tmp_dir_name: str = create_tmp_dir()
    move_all_files_to_tmp_dir(directory_path, tmp_dir_name, flat_dir)

    tmp_dir_path = os.path.join(directory_path, tmp_dir_name)
    # files is a list of tuples with three elements
    # [path_of_currently_parsed_directory, list_of_subdirectory_names, filenames_of_current_dir]
    dir_structure: list[(str, list[str], list[str])] = list(os.walk(tmp_dir_path, topdown=False))
    all_file_details: list[FileDetails] = get_all_file_details(dir_structure)

    sort_all_file_details(sort_by_date, all_file_details)

    filename_generator = CounterPrefixPathGenerator(directory_path, keep_filenames, start_number)
    for file_detail in all_file_details:
        new_file_path = filename_generator.next(file_detail)
        # copy files to main dir
        shutil.copy2(file_detail.full_path, new_file_path)

    restore_backup_message = "Do you want to delete backup directory?"
    if query_yes_no(restore_backup_message):
        delete_backup(tmp_dir_name)


def get_path_of_current_dir() -> str:
    return os.getcwd()


def create_tmp_dir() -> str:
    tmp_dir_name = "temp"
    while os.path.isdir(tmp_dir_name):
        tmp_dir_name = tmp_dir_name + "_1"
    os.mkdir(tmp_dir_name)
    return tmp_dir_name


def is_python_file(filename: str) -> bool:
    return filename.endswith(".py")


def move_all_files_to_tmp_dir(original_dir_path: str, tmp_dir_name: str, move_dirs: bool) -> None:
    for filename in os.listdir(original_dir_path):
        if move_dirs:
            if not is_python_file(filename):
                shutil.move(filename, tmp_dir_name)
        else:
            if os.path.isfile(filename) and not is_python_file(filename):
                shutil.move(filename, tmp_dir_name)


# if text is only digits it returns its integer representation, otherwise returns just text
def ascii_to_int(text):
    return int(text) if text.isdigit() else text


def natural_human_sorting(text):
    return [ascii_to_int(c) for c in re.split(r'(\d+)', text)]


def get_all_file_details(dir_structure: list[(str, list[str], list[str])]) -> list[FileDetails]:
    return [FileDetails(filename, dir_of_file)
            for dir_of_file, _, filenames_list in dir_structure
            for filename in filenames_list
            if not is_python_file(filename)]


def sort_all_file_details(sort_by_date: bool, all_file_details: list[FileDetails]) -> None:
    if sort_by_date:
        all_file_details.sort(key=lambda file_details: file_details.get_modification_time())
    else:
        all_file_details.sort(key=lambda file_details: natural_human_sorting(file_details.filename))


def remove_subdirectories(all_sub_dirs_paths_to_delete: list[str]):
    for path in all_sub_dirs_paths_to_delete:
        os.rmdir(path)


def delete_backup(tmp_dir_name: str):
    shutil.rmtree(os.path.join(get_path_of_current_dir(), tmp_dir_name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="filename_changer",
                                     usage="filename_changer [options]",
                                     description="Change all filenames in dir to next integers."
                                                 " With flag [-f --flat] will also move all "
                                                 "files from subdirectories to main directory."
                                                 " Files can be sorted by name (default) or modification date. [-d --date]"
                                                 " It is also possible to start numeration "
                                                 "form specified number [-s --start]", )

    # -f FLATTEN_DIR, -s START_FROM def 0, -d BY_DATE_NOT_ALPHABETICALLY
    parser.add_argument("-f", "--flat", dest="flat", action='store_true',
                        help="Place items from sub-dirs into main dir (def False)")
    parser.add_argument("-s", "--start", dest="start", metavar="[integer]", default=1, type=int,
                        help="First filename - integer (def 1)")
    parser.add_argument("-d", "--date", dest="by_date", action='store_true',
                        help="Sort filenames by creation date not alphabetically")
    parser.add_argument("-k", "--keep", dest="keep_current_filenames", action='store_true',
                        help="Keep current filenames and only add prefix like '1_'")

    args = parser.parse_args()
    confirm_message = "Wanna run with the following options: " + args.__str__().removeprefix("Namespace")

    if query_yes_no(confirm_message):
        change_filenames(flat_dir=args.flat, sort_by_date=args.by_date, start_number=args.start, keep_filenames=args.keep_current_filenames)

    # change_filenames(True, False, 200, True)
